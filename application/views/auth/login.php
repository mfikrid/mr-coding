<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/bootstrap-grid.css">
    <link rel="stylesheet" href="assets/library/fontawesome/css/fontawesome.css">
    <link rel="stylesheet" href="assets/library/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/library/fontawesome/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body class="background-grey">
    <section>
        <div>
            <nav>
                <a class="navbar-brand">
                    <img src="assets/img/panah.png" width="100" alt="">
                </a>
                <div class="img-fluid">
                    <img src="assets/img/animaslogin.png" class="image-fluid mb-n4" width="40%" align="right" alt="">
                </div>
            </nav>
            <div class="container">
                <h1 class="text-brown"><b>Daftar</b></h1><br>
                <form class="form-inline my-2 my-lg-2">
                    <button class="btn background-white my-2 my-sm-0 text-google"><i class="fa-brands fa-google"></i><b>&emsp;Log in with Google</b></button>
                </form><br><br>
            </div>
            <section class="jumbotron container">
                <div class="container align-items-center m-lg-0">
                    <p>
                        <input type="text" name="username" placeholder="name@gmail.com">
                    </p>
                    <p>
                        <input type="password" name="password" placeholder="Enter your password" />
                    </p>

                    <h6 class="btn" style="color: rgb(0, 0, 0)" href="#">Forgot Password? </h6>
                    <form class="form-inline my-2 my-lg-2">
                        <button class="btn background-browny3 my-2 my-sm-0 text-white">Daftar <i class="fas fa-user"></i></button>
                    </form>
                    <p>
                        don’t have an account yet? <a class="btn" href="#">Sign up</a>
                    </p>
                </div>
            </section>
        </div>
    </section>







    <script src="assets/js/jquery.slim.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/library/fontawesome/js/fontawesome.js"></script>
    <script src="assets/library/fontawesome/js/all.js"></script>
</body>

</html>