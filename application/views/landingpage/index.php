<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mr.Coding</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/bootstrap-grid.css">
    <link rel="stylesheet" href="assets/library/fontawesome/css/fontawesome.css">
    <link rel="stylesheet" href="assets/library/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/library/fontawesome/css/all.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body class="background-brown">
    <!--Kontak Person-->
    <nav class="navbar navbar-expand-lg body">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="assets/img/logo.png" width="100" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-lg-auto">
                    <li class="nav-item">
                        <a class="nav-link active text-decoration-underline text-danger" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Berlangganan
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#">Paket 1 Bulan</a></li>
                            <li><a class="dropdown-item" href="#">Paket 3 Bulan</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="#">Pilih Paket Lainnya</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white" href="#">Tentang Kami</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-2">
                    <button class="btn background-orange my-2 my-sm-0 text-white"><i class="fas fa-square-pen"></i> Daftar</button>
                </form>
            </div>
        </div>
    </nav>
    <!--Gambar Unit-->
    <section class="jumbotron px-4 py-4">
        <div class="row align-items-center m-lg-4">
            <div class="col-sm text-white">
                <h3>Mr Coding <br>Hadir...</h3>
                <br>
                <p>Mulai daftar disini sekarang juga supaya kamu<br> mendapatkan kelas dasar secara gratis untuk<br> pengguna baru!</p>
                <a href="#" class="btn background-orange text-white">Daftar <i class="fas fa-arrow-down"></i></a>
            </div>
            <div class="col-sm text-white">
                <img src="assets/img/animasi.png" class="image-fluid mb-n4" width="90%" alt="">
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="container p-5">
                <div class="rounded shadow background-browny3 py-4 px-3 mt-4">
                    <div class="row">
                        <div class="col-md-4 d-flex align-content-center">
                            <div class="container">
                                <img src="assets/img/mentor.png" alt=""><br>
                                <div class="media text-left">
                                    <div class="media-body">
                                        <b class="mt-0 text-white">Mentor Terbaik</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 d-flex align-content-center">
                            <div class="container py-3">
                                <img src="assets/img/biaya.png" alt=""><br>
                                <div class="media text-left">
                                    <div class="media-body">
                                        <b class="mt-0 text-white">Biaya Murah</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 d-flex align-content-center">
                            <div class="container">
                                <p>
                                    <img src="assets/img/materi.png" alt=""><br>
                                </p>
                                <div class="media text-left">
                                    <div class="media-body">
                                        <b class="mt-0 text-white">Materi Mudah Dipahami</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <section class="py-4">
        <div class="container text-left">
            <h4 class="display-5 my-4 text-center text-white">Beragam RoadMap Belajar</h4>
            <div class="row">
                <div class="col-md-4">
                    <div class="card rounded-item background-browny py-4 px-2">
                        <img src="assets/img/reactjs.png" alt="">
                        <div class="media px-2 py-3 text-left">
                            <div class="media-body">
                                <b class="mt-0 text-white">React JS</b>
                                <p class="text-brown">Rp. 299.000</p>
                                <form class="form-inline my-2 my-lg-2">
                                    <button class="btn background-browny3 my-2 my-sm-0 text-white">Beli</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card rounded-item background-browny py-4 px-2">
                        <img src="assets/img/js.png" alt="">
                        <div class="media px-2 py-3 text-left">
                            <div class="media-body">
                                <b class="mt-0 text-white">Javascript</b>
                                <p class="text-brown">Rp. 149.000</p>
                                <form class="form-inline my-2 my-lg-2">
                                    <button class="btn background-browny3 my-2 my-sm-0 text-white">Beli</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card rounded-item background-browny py-4 px-2">
                        <img src="assets/img/tailwind.png" alt="">
                        <div class="media px-2 py-3 text-left">
                            <div class="media-body">
                                <b class="mt-0 text-white">Tailwind CSS</b>
                                <p class="text-brown">Rp. 199.000</p>
                                <form class="form-inline my-2 my-lg-2">
                                    <button class="btn background-browny3 my-2 my-sm-0 text-white">Beli</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/js/jquery.slim.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/library/fontawesome/js/fontawesome.js"></script>
    <script src="assets/library/fontawesome/js/all.js"></script>
</body>

</html>